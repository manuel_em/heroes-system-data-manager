var conf = require("./conf"),
	i18n = require("bgt-i18n"),
	routesVarious = require("./routes/various");

//require("bgt-user");


var Module = {

	options : null,

	init : function (options) {
		if (typeof options === "object") {
			if ("path" in options) conf.modulepath = options.path;
			if ("staticpath" in options) conf.staticpath = options.staticpath;
			else conf.staticpath = conf.modulepath + "/static";
			if ("title" in options) conf.title = options.title;
			if ("locales" in options) conf.locales = options.locales;
		}
		this.options = options;

		i18n.init({
			app : this.options.app,
			url_path : conf.modulepath + "*",
			locales : conf.locales
		});

		var app = this.options.app;

		console.log(conf.modulepath);

		//app.use(conf.modulepath + "/static", express.static(__dirname + '/static'));

		app.get(conf.modulepath, routesVarious.index);
	},

	apiRest : function () {
		var app = this.options.app;
	}

};

module.exports = Module;