module.exports = {
	title : "Heroes system data manager",
	viewspath : __dirname + "/views/",
	layoutspath : __dirname + "/views/layouts/",
	modulepath : "/hsdm",
	staticpath : "/hsdm/static",
	locales : ["fr", "en"]
};